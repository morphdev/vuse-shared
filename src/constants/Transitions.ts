export const transitionTypes = [
  'CrossDissolve',
  'Swipe',
  'Pan Left',
  'BoundingUp',
  'Fade'
] as const;
export type Transitions = typeof transitionTypes[number];