export interface Address {
    city: string;
    state: string;
    street: string;
    streetTwo: string;
    zip: string;
    country: string;
    neighborhood?: string;
}
export default Address;