
import {Address} from './Address';
import Reference from './Reference';
import Device from './Device';

export const roles = ['admin', 'enterprise', 'enterpriseadmin', 'user'] as const;
type Role = typeof roles[number];

export const propertyTypes = ['rental', 'purchase'] as const;
type PropertyType = typeof propertyTypes[number];

export const subcriptionOptions = ['IAP', 'freetrial', 'enterprise'] as const;
type SubscriptionOption = typeof subcriptionOptions[number];

export const userDescriptions = ['relatorWithMultipleListings', 'relatorWithOneListing'] as const;
type UserDescription = typeof userDescriptions[number];

export interface NotificationSettings {
    offers: boolean;
    postRecommendations: boolean;
    videoUpdates: boolean;
}

export interface UserData {
    shouldSubscribe?: boolean;
    active: boolean;
    address?: Address;
    brandLogoUrl?: string;
    profileImageUrl?: string;
    currency?: string;
    metricEnabled: boolean;
    firstName: string;
    lastName: string;
    lastIp?: string;
    phone?: string;
    preferredPropertyType?: PropertyType;
    preferredStyle?: Reference;
    enterprise?: Reference;
    role: Role;
    profileEmail?: string;
    companyName?: string;
    dreNumber?: string;
    subcription?: SubscriptionOption;
    subscriptionOverride?: boolean;
    userDescription?: UserDescription;
    notifications?: NotificationSettings;
    devices?: Device[];
    changePasswordRequired?: boolean;
    creationDate?: Date;
}
export default UserData;