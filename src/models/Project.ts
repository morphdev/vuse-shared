import Address from './Address';
import Video from './Video';

export interface ProjectPrice {
  unit: string;
  value: string;
}

export interface ProjectPropertySize {
  unit: string;
  value: string;
}

export interface ProjectLotSize {
  unit: string;
  value: string;
}

export interface Project {
  name: string;
  bedrooms?: string;
  bathrooms?: string;
  address?: Address;
  price?: ProjectPrice;
  propertySize?: ProjectPropertySize;
  lotSize?: ProjectLotSize;
  categories: string[];
  videos?: Video[];
  creationDate: Date;
}
export default Project;