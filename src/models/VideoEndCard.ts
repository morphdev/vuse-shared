export const endCardLabelType = ['firstName', 'lastName', 'name', 'email', 'phone', 'secondaryText', 'static', 'company', 'dreNumber'] as const;
export type EndCardLabelType = typeof endCardLabelType[number];

export interface ColorType {
    type: 'static' | 'theme';
    color: string;
}

export interface VideoEndCardImage {
    type: 'profile' | 'brand' | 'static',
    color?: ColorType,
    imageUrl?: string,
    x: number,
    y: number,
    scale?: 'scaleAspectFill' | 'scaleAspectFit' | 'scaleToFill',
    borderRadius?: number,
    size: {
        width: number,
        height: number
    }
}

export interface VideoEndCardLabel {
    type: EndCardLabelType,
    color?: ColorType,
    fontName?: string,
    fontSize?: number,
    x: number,
    y: number,
    text?: string,
    lines?: number
}

export interface VideoEndCard {
    name: string;
    format: 'landscape' | 'portrait' | 'square';
    backgroundColor: ColorType,
    duration: number,
    images?: VideoEndCardImage[],
    labels?: VideoEndCardLabel[]
}

export default VideoEndCard;