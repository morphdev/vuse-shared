export const animationTypes = ['Fade', 'SlideInFromRight', 'SlideInFromLeft', 'SlideInFromTop', 'SlideInFromBottom'] as const;
export type AnimationType = typeof animationTypes[number];
export default AnimationType;