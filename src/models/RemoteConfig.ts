export interface RemoteConfig {
    navigationProfileTitle: string;
    navigationTutorialTitle: string;
    navigationTutorialImageUrl: string;
    navigationProfileImageUrl: string;
    includeEndAnimation: string;
    freeTrialPeriod: string;
    subscriptionIapPageTitle: string;
    subscriptionIapPageSubtitle: string;
    subscriptionBrandsPageTitle: string;
    subscriptionBrandsPageSubtitle: string;
}