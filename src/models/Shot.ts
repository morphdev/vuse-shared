import Reference from "./Reference";
import { Transitions } from '../constants';
import Animation from './Animation';

export const shotTypes = ['topLeft' ,'topCenter' ,'topRight' ,'middleLeft' ,'middleCenter' ,'middleRight' ,'bottomLeft' ,'bottomCenter' ,'bottomRight'] as const;
export type ShotType = typeof shotTypes[number];

export const cameraTypes = ['front' ,'back'] as const;
export type CameraType = typeof cameraTypes[number];

export interface Shot {
  name: string;
  id: string;
  order: number;
  canUsePhoto: boolean;
  canUseVideo: boolean;
  defaultDuration: number;
  thumbnailUrl?: string;
  exampleVideoUrl?: string;
  instructions?: string;
  labelLocation: ShotType;
  audioVolume?: number;
  musicVolume?: number;
  defaultCamera: CameraType;
  labelSpacing?: {
    verticalMargin: number;
    horizontalMargin: number;
    verticalPadding: number;
    horizontalPadding: number;
  };
  labelAnimation?: Animation;
  labelInverted?: boolean;
  labelBackgroundColorOpacity?: number;
  labelFontSize?: number;
  labelFontName?: string;
  labelBorder?: {
    borderColor: string;
    borderWidth: number;
    borderRadius: number;
  }
  maxDuration: number;
  stockFootage?: Reference;
  kenBurnsStartScale?: number;
  kenBurnsEndScale?: number;
  transition?: Transitions;
}
export default Shot;