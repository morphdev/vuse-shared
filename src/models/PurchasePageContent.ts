export interface PurchasePageImage {
  order: number;
  url: string;
}

export interface PurchasePageContent {
  checklist: {
    text: string;
    boldText: string;
    boldFirst: boolean;
  }[];
  version: string;
  isIAPPage: boolean;
  title: string;
  images: PurchasePageImage[];
}
export default PurchasePageContent;