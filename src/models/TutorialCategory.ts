import Tutorial from './Tutorial';

export interface TutorialCategory {
  isPublished: boolean;
  name: string;
  order: number;
  tutorials?: Tutorial[];
}
export default TutorialCategory;