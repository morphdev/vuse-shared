export interface Reference {
    id: string;
    path: string;
}
export default Reference;