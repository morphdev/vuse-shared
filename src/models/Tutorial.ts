export interface Tutorial {
  name: string;
  duration: number;
  order: number;
  thumbnailUrl: string;
  videoUrl: string;
  isPublished: boolean;
}
export default Tutorial;