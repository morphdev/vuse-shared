import { Fonts, Transitions } from '../constants';
import Reference from './Reference';

export interface Style {
  isPublished: boolean;
  isGlobal: boolean;
  name: string;
  order: number;
  font: keyof typeof Fonts;
  thumbnailUrl: string;
  transition?: Transitions;
  exampleVideoUrl?: string;
  enterprise?: Reference;
}
export default Style;