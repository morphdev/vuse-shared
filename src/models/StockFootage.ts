import Reference from "./Reference";

export interface StockFootage {
  isVideo: boolean;
  isGlobal: boolean;
  thumbnailUrl: string;
  url: string;
  landscapeUrl: string;
  squareUrl: string;
  name: string;
  order: number;
  isPublished: boolean;
  tags?: string[];
  enterprise?: Reference;
}
export default StockFootage;