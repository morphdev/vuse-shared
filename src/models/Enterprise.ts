import Reference from './Reference';
import Color from './Color';

export interface Enterprise {
  allowedStyles?: Reference[];
  allowedTemplates?: Reference[];
  userAllowedStyles?: Reference[];
  userAllowedTemplates?: Reference[];
  colors?: Color[];
  logo: string;
  name: string;
  shouldLimitStyles: boolean;
  shouldLimitTemplates: boolean;
  users?: Reference[];
  theme?: {
    name: string,
    palette: {
      type: 'light' | 'dark',
      action: {
        active: string
      },
      background: {
        default: string,
        dark: string,
        paper?: string
      },
      primary: {
        main: string
      },
      secondary: {
        main: string
      },
      error: {
        main: string
      },
      text: {
        primary: string,
        secondary: string
      }
    }
  },
}
export default Enterprise;