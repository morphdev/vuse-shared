import Template from './Template';

export interface TemplateCategory {
  isPublished: boolean;
  name: string;
  order: number;
  iconUrl: string;
  description?: string;
  previewVideoUrl?: string;
  templates?: Template[];
}
export default TemplateCategory;