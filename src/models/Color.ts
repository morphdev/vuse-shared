export interface ColorValue {
    r: number;
    g: number;
    b: number;
    a: number;
}

export interface Color {
    isPublished: boolean;
    name: string;
    backgroundColor: ColorValue;
    primaryColor: ColorValue;
    secondaryColor: ColorValue;
    primaryTwoColor: ColorValue;
    secondaryTwoColor: ColorValue;
}
export default Color;