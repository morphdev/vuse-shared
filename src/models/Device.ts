export interface Device {
  name: string;
  ip: string;
  uid: string;
  mostRecent?: boolean;
  pushNotificationId?: string;
}
export default Device;