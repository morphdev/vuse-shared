import Color from './Color';
import Reference from './Reference';
import Shot from './Shot';
import Song from './Song';
import {TextOverlay} from './Template';

export interface VideoToggles {
  agentId: boolean;
  brand: boolean;
  music: boolean;
  price: boolean;
  watermark: boolean;
}

export interface VideoShot {
  shot: Shot;
  order: number;
  text?: string | null;
  localUrl?: string | null;
  stockFootageUrls?: {
    format: string,
    url: string,
    localUrl: string
  }[];
  isImage: boolean;
  localThumbnailUrl?: string;
}

export interface Video {
  id: string;
  name: string;
  format: 'landscape' | 'portrait' | 'square';
  style: Reference;
  template: Reference;
  color?: Reference;
  colorObject?: Color;
  song?: Song | null;
  songUrl?: string | null;
  voiceOverUrl?: string | null;
  thumbnailUrl?: string | null;
  filePath?: string | null;
  toggles: VideoToggles;
  videoShots: VideoShot[];
  creationDate: Date;
  textOverlayOverrides?: TextOverlay[];
  measurements?: {
    hasReachedSharePage: boolean;
  }
  settings?: {
    includeEndAnimation: boolean;
    brandingOverlay: boolean;
    profileSlide: boolean;
    text: boolean;
    music: boolean;
    propertyPrice: boolean;
  }
}
export default Video;
