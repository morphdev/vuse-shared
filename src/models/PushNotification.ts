import Reference from "./Reference";
import { roles } from ".";

type Role = typeof roles[number];

export const equalityTypes = ['moreThan' , 'lessThan', 'between', 'equals'] as const;
type EqualityType = typeof equalityTypes[number];

export const responsiveNotificationTypes = ['notVisitedSharePage', 'startNewProject'] as const;
type ResponsiveNotificationType = typeof responsiveNotificationTypes[number];

type DateCompareType = {
    condition: EqualityType;
    daysA: number;
    daysB?: number;
}

export interface PushNotificationCondition {
    isAndroid?: boolean;
    isIos?: boolean;
    lastEngagement?: DateCompareType;
    firstOpen?: DateCompareType;
    appVersion?: {
        condition: EqualityType;
        value: string;
    }
}

export interface PushNotificationTargeting {
    allUsers?: boolean;
    nonEnterpriseUsers?: boolean;
    enterprise?: Reference[];
    selectedUsers?: Reference[];
}

export interface PushNotification {
    name: string;
    title: string;
    text: string;
    hasSent: boolean;
    triggerDate: any;
    imageUrl?: string;
    targeting?: PushNotificationTargeting;
    conditions?: PushNotificationCondition[];
    author?: Reference;
}

export interface ResponsiveNotification {
    name: string;
    type: ResponsiveNotificationType;
    value?: string;
    dateOffset: number; // In minutes
    title: string;
    text: string;
    imageUrl?: string;
    targeting?: PushNotificationTargeting;
}

export interface ScheduledReponsiveNotification {
    id: string;
    responsiveNotification: Reference;
    date: Date;
}