import Song from './Song';
import Shot, { ShotType } from './Shot';
import Reference from './Reference';
import Animation from './Animation';

export interface TextOverlay {
  type: 'price' | 'address' | 'beds' | 'baths' | 'bedbath' | 'squarefootageAndLotSize' | 'lotSize' | 'squarefootage' | 'static';
  startTime: number;
  duration: number;
  location: ShotType;
  text?: string;
  fontName?: string,
  fontSize?: number,
  horizontalMargin?: number,
  verticalMargin?: number,
  horizontalPadding?: number,
  verticalPadding?: number,
  backgroundColor?: string,
  backgroundColorOpacity?: number,
  inverted?: boolean,
  animation?: Animation,
  border?: {
    borderColor: string;
    borderWidth: number;
    borderRadius: number;
  }
}

export interface Template {
  includeEndAnimation?: boolean;
  isPublished: boolean;
  isGlobal: boolean;
  renderMode: 'fullscreen' | 'shrunken';
  defaultFormat: 'landscape' | 'portrait' | 'square';
  name: string;
  order: number;
  maxShots: number;
  thumbnailUrl: string;
  previewVideoUrl: string;
  defaultStyle?: Reference;
  recommendedStockFootage?: Reference[];
  recommendedTemplates?: Reference[];
  colors?: Reference[];
  defaultColor?: Reference;
  songs?: Song[];
  shots?: Shot[];
  textOverlays?: TextOverlay[];
  enterprise?: Reference;
  endCards?: Reference[];
  hasAddressQuestions?: boolean;
  hasPropertyQuestions?: boolean;
  hasShotQuestions?: boolean;
}
export default Template;