export interface Song {
  name: string;
  default?: boolean;
  songId: string;
  artist: {
    name: string;
    image: string;
  };
  duration?: number;
  bpm?: number;
  energy?: string;
  characteristic?: string[];
  genre?: string[];
  instrument?: string[];
  mood?: string[];
}
export default Song;